import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome'
import InputBox from '../../components/InputBox/InputBox'
import DropDownBox from '../../components/DropDownBox/DropDownBox'
import style from './style.less';

const listCountries1 = [
  { id: 0, title: 'Algeria', key: 0  },
  { id: 1, title: 'Barbados', key: 1  },
  { id: 2, title: 'Kenya', key: 2  },
]

const listCountries2 = [
  { id: 0, title: 'Laos', key: 0  },
  { id: 1, title: 'Maldives', key: 1  },
  { id: 2, title: 'Saint Lucia', key: 2  },
]

class AddressDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      address: props.address,
    }
  }

  componentWillReceiveProps(nextProps) {
      this.setState({ address: nextProps.address });
  }

  onAddressChanged = () => {
    if (this.props.onAddressChanged) {
      this.props.onAddressChanged(this.state.address);
    }
  }

  onChangeAddressPart = (value, fieldName) => {
    if (value !== this.state.address[fieldName]) {
      const address = { ...this.state.address };
      address[fieldName] = value;

      this.setState({ address }, this.onAddressChanged);
    }
  }

  render() {
    return <div className={style.wrapper}>
      <div><FontAwesome  name="angle-down" size="2x" className={style.iconSize} /></div>
      <div className={style.marginIcon}><FontAwesome  name="angle-down" size="2x" className={style.iconSize2} /></div>
      <div className={style.line}>
        <div className={style.left}>
          <div className={style.info}>Address line 1</div>
          <InputBox text={this.state.address.addressLine1} onBlur={(value) => {this.onChangeAddressPart(value, `addressLine1`)}} />
        </div>
        <div className={style.right}>
          <div className={style.info}>Town</div>
          <InputBox text={this.state.address.town}  onBlur={(value) => {this.onChangeAddressPart(value, `town`)}} />
        </div>
      </div>
      <div className={style.line}>
        <div className={style.left}>
          <div className={style.info}>Address line 2</div>
          <InputBox text={this.state.address.addressLine2} onBlur={(value) => {this.onChangeAddressPart(value, `addressLine2`)}} />
        </div>
        <div className={style.right}>
          <div className={style.info}>Country</div>
          <DropDownBox title={this.state.address.country1} list={listCountries1} onSelect={(value) => {this.onChangeAddressPart(value.title, `country1`)}} />
        </div>
      </div>
      <div className={style.line}>
        <div className={style.left}>
          <div className={style.info}>Address line 3</div>
          <InputBox text={this.state.address.addressLine3} onBlur={(value) => {this.onChangeAddressPart(value, `addressLine3`)}} />
        </div>
        <div className={style.right}>
          <div className={style.info}>Country</div>
          <DropDownBox title={this.state.address.country2} list={listCountries2} onSelect={(value) => {this.onChangeAddressPart(value.title, `country2`)}} />
        </div>
      </div>
    </div>;
  }
}

AddressDetails.propTypes = {
  onAddressChanged: PropTypes.func,
  address: PropTypes.object
};

export default AddressDetails;
