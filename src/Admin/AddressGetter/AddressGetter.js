import React, { Fragment } from 'react';
import Button from '../components/Button/Button';
import CurrentAddress from './CurrentAddress/CurrentAddress'
import SelectAddress from './SelectAddress/SelectAddress'
import AddressDetails from './AddressDetails/AddressDetails'
import ResultPanel from './ResultPanel/ResultPanel';
import dataService from './services/dataService';
import style from './style.less';

class AddressGetter extends React.Component {
  constructor() {
    super();

    this.state = {
      stayingPeriod: { years: `1 year`, months: `1 month`},
      searchingZipCode: null,
      addresses: null,
      selectedAddress: null,
      finished: false,
      requestError: null
    }
  }

  onZipCodeChanged = (zipCode) => {
    this.setState({requestError: undefined, searchingZipCode: zipCode});
    const getAddress = zipCode ? dataService.getAddresses(zipCode) : dataService.getTestData();

    getAddress.then(
      response => {
        this.setState({addresses: response, requestError: undefined, selectedAddress: undefined, finished: false });
      },
      error => {
        this.setState({addresses: undefined, requestError: error.message || `request error`, selectedAddress: undefined, finished: false })
      }
    );
  };

  onStayingPeriodChanged = (stayingPeriod) => {
    this.setState({stayingPeriod: stayingPeriod, addresses: undefined, selectedAddress: undefined});
  };

  onAddressSelected = (selectedAddress) => {
    this.setState( {selectedAddress, finished: true } );
  }

  onSelectedAddressChanged = (address) => {
    this.setState({selectedAddress: {...address}});
  }

  onFinished = () => {
    this.setState({ finished: false, addresses: null });
  }

  renderResultPanel() {
    if (this.state.selectedAddress && !this.state.finished) {
      return <ResultPanel address={this.state.selectedAddress} stayingPeriod={this.state.stayingPeriod}/>
    }
    return null;
  }

  renderSelectAddress() {
    if (this.state.addresses) {
      return <SelectAddress addresses={this.state.addresses} onAddressSelected={this.onAddressSelected} />;
    }
    return null;
  }

  renderAddressDetails() {
    if (this.state.selectedAddress && this.state.finished) {
      return <AddressDetails address={this.state.selectedAddress} onAddressChanged={this.onSelectedAddressChanged}/>
    }
    return null;
  }

  render() {
    return <Fragment>
      <div className={style.topBlock}></div>
      <div className={style.mainBlock}>
        <div className={style.greeting}>Home address</div>
        <div>Please enter the director’s home address for the last 3 years.</div>
        <div className={style.splitter}></div>
        {this.renderResultPanel()}
        <div>
          <CurrentAddress
            years={this.state.stayingPeriod.years}
            months={this.state.stayingPeriod.months}
            onZipCodeChanged={this.onZipCodeChanged}
            onStayingPeriodChanged={this.onStayingPeriodChanged}
            error={this.state.requestError}
            searchingZipCode={this.state.searchingZipCode}
          />
        </div>
        {this.renderSelectAddress()}
        {this.renderAddressDetails()}
        <div className={style.continueBtn}>
          <Button text={`Confirm and continue`} disabled={!this.state.finished} onClick={this.onFinished} /></div>
      </div>
    </Fragment>;
  }
}

export default AddressGetter;
