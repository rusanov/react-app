import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import style from './style.less';
import SearchBox from '../../components/SearchBox/SearchBox'
import DropDownBox from '../../components/DropDownBox/DropDownBox'

const listMonths = [
  { id: 0, title: '0 months', key: 0  },
  { id: 1, title: '1 month', key: 1  },
  { id: 2, title: '2 months', key: 2  },
  { id: 3, title: '3 months', key: 3  },
  { id: 4, title: '4 months', key: 4  },
  { id: 5, title: '5 months', key: 5  },
  { id: 6, title: '6 months', key: 6  },
  { id: 7, title: '7 months', key: 7  },
  { id: 8, title: '8 months', key: 8  },
  { id: 9, title: '9 months', key: 9  },
  { id: 10, title: '10 months', key: 10  },
  { id: 11, title: '11 months', key: 11  },
  { id: 12, title: '12 months', key: 12  }
]

const listYears = [
  { id: 0, title: '0 years', key: 0  },
  { id: 1, title: '1 year', key: 1  },
  { id: 2, title: '2 years', key: 2  },
  { id: 3, title: '3 years', key: 3  },
  { id: 4, title: '4 years', key: 4  },
  { id: 5, title: '5 years', key: 5  },
  { id: 6, title: '6 years', key: 6  },
  { id: 7, title: '7 years', key: 7  },
  { id: 8, title: '8 years', key: 8  },
  { id: 9, title: '9 years', key: 9  },
  { id: 10, title: '10 years', key: 10  },
]

class CurrentAddress extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      years: props.years,
      months: props.months,
      error: props.error
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.error !== nextProps.error || this.props.years !== nextProps.years || this.props.months !== nextProps.months ) {
      this.setState({ years: nextProps.years, months: nextProps.months, error: nextProps.error });
    }
  }

  onSearch = (e) => {
    if (this.props.onZipCodeChanged) {
      this.props.onZipCodeChanged(e);
    }
  }

  onYearPeriodChanged = (item) => {
    this.setState({years: item.title}, this.notifyPeriodChanged);
  }

  onMonthPeriodChanged = (item) => {
    this.setState({months: item.title}, this.notifyPeriodChanged);
  }

  notifyPeriodChanged = () => {
    if (this.props.onStayingPeriodChanged) {
      const { years, months } = this.state;
      this.props.onStayingPeriodChanged({ years, months });
    }
  }

  render() {
    return <div className={style.wrapper}>
      <div className={style.info}>How long did you stay at your <strong>current address</strong>?</div>
      <div className={style.staying_time}>
        <span className={style.drop_down_box}>
          <DropDownBox title={this.state.years} list={listYears} onSelect={this.onYearPeriodChanged} />
        </span>
        <span className={style.divider} />
        <span className={style.drop_down_box}>
          <DropDownBox title={this.state.months} list={listMonths} onSelect={this.onMonthPeriodChanged} />
        </span>
      </div>
      <div className={style.info}>Your address:</div>
      <div className={style.search_box}>
        <SearchBox
          placeholder={`Please enter zip code (leave empty to get test data)`}
          onSearch={this.onSearch}
          text={this.props.searchingZipCode}
        />
      </div>
      {this.renderError()}
    </div>;
  }

  renderError() {
    if (this.state.error) {
      return <div className={style.error}>{this.state.error}</div>;
    }
  }
}

CurrentAddress.propTypes = {
  onZipCodeChanged: PropTypes.func,
  onStayingPeriodChanged: PropTypes.func,
  years: PropTypes.string,
  months: PropTypes.string,
  searchingZipCode: PropTypes.string,
  error: PropTypes.string
};

export default CurrentAddress;
