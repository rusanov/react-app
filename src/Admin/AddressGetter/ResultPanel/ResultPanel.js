import React from 'react';
import PropTypes from 'prop-types';
import style from './style.less';

class ResultPanel extends React.PureComponent {
  render() {
    const { addressLine1, addressLine2, addressLine3, town, country1, country2 } = this.props.address;
    const { years, months } = this.props.stayingPeriod;

    return <div className={style.wrapper}>
      <div className={style.content}>
        <div>{addressLine1}, {addressLine2}, {addressLine3}, {town}, {country1}, {country2}</div>
        <div>Time at address: {years}, {months}</div>
      </div>
    </div>;
  }
}

ResultPanel.propTypes = {
  address: PropTypes.object,
  stayingPeriod: PropTypes.object,
};

export default ResultPanel;
