import React from 'react';
import PropTypes from 'prop-types';
import style from './style.less';
import DropDownBox from '../../components/DropDownBox/DropDownBox'

class SelectAddress extends React.PureComponent {
  onAddressSelected = (item) => {
    if (this.props.onAddressSelected) {
      this.props.onAddressSelected(item);
    }
  }

  render() {
    return <div className={style.wrapper}>
      <div className={style.info}>Select your address</div>
          <DropDownBox title={'Select your address'} list={this.props.addresses} onSelect={this.onAddressSelected} />
      </div>;
  }
}

SelectAddress.propTypes = {
  onAddressSelected: PropTypes.func,
  addresses: PropTypes.array
};

export default SelectAddress;
