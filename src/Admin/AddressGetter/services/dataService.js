import { get } from '../../helpers/httpClient';

const url = {
  getAddress: 'https://api.getaddress.io/find/'
};

const data = {
  "api-key": 'NAewTP_zPkWdaeDeVl07sA15595'
}

// result for query NW8 8EP
const testData = {"latitude":51.5243468,"longitude":-0.1691675,"addresses":["12a-14a Church Street, , , , , London, ","16a Church Street, , , , , London, ","18a-18b Church Street, , , , , London, ","20 Church Street, , , , , London, ","28a Church Street, , , , , London, ","36 Church Street, , , , , London, ","Antiques, 22 Church Street, , , , London, ","Antiques Shop, 48 Church Street, , , , London, ","Basement Flat, 18 Church Street, , , , London, ","Basement Flat, 46 Church Street, , , , London, ","Cristobal, 26 Church Street, , , , London, ","D & A Binder, 34 Church Street, , , , London, ","Deuxieme Ltd, 44 Church Street, , , , London, ","Flat, 20 Church Street, , , , London, ","Flat, 22 Church Street, , , , London, ","Flat, 26 Church Street, , , , London, ","Flat, 48 Church Street, , , , London, ","Flat, 50 Church Street, , , , London, ","Flat, 52 Church Street, , , , London, ","Flat 1, 32 Church Street, , , , London, ","Flat 1, 38 Church Street, , , , London, ","Flat 10, 32 Church Street, , , , London, ","Flat 11, 32 Church Street, , , , London, ","Flat 1-3, 24 Church Street, , , , London, ","Flat 2, 32 Church Street, , , , London, ","Flat 2, 38 Church Street, , , , London, ","Flat 3, 32 Church Street, , , , London, ","Flat 3, 38 Church Street, , , , London, ","Flat 4, 32 Church Street, , , , London, ","Flat 4, 38 Church Street, , , , London, ","Flat 5, 32 Church Street, , , , London, ","Flat 5, 38 Church Street, , , , London, ","Flat 6, 32 Church Street, , , , London, ","Flat 6, 38 Church Street, , , , London, ","Flat 7, 32 Church Street, , , , London, ","Flat 7, 38 Church Street, , , , London, ","Flat 8, 32 Church Street, , , , London, ","Flat 8, 38 Church Street, , , , London, ","Flat 9, 32 Church Street, , , , London, ","Flat 9, 38 Church Street, , , , London, ","Flat A-C, 46 Church Street, , , , London, ","Gallery 1930, 18 Church Street, , , , London, ","Harvey (Management Services) Ltd, 42 Church Street, , , , London, ","Hudson Grove, 14 Church Street, , , , London, ","Joes, 28 Church Street, , , , London, ","Lamia Salon, 46 Church Street, , , , London, ","Marchand Antiques, 40 Church Street, , , , London, ","Schmidmcdonagh, 24 Church Street, , , , London, ","Shtoura, 16 Church Street, , , , London, ","Snipping Image Hairdressing, 50 Church Street, , , , London, ","The Traders Inn, 52 Church Street, , , , London, ","Vincenzo Caffarella, 30 Church Street, , , , London, ","Young & Son, 12 Church Street, , , , London, "]};

const dataService = {
  getAddresses(zipCode) {
    return get(url.getAddress + zipCode, data).then(response => {return mapData(response)});
  },

  getTestData() {
    return Promise.resolve(testData).then(response => {return mapData(response)});
  }
};

function mapData(response) {
  if(response.addresses && response.addresses.length) {
    const result = response.addresses.map((item, index) => {
      const parts = item.split(',');
      return {
        id: index,
        title: item.split(',').toString(),
        key: index,
        addressLine1: parts[0],
        addressLine2: parts[1],
        addressLine3: parts[2],
        country1: parts[3],
        country2: parts[4],
        town: parts[5]
      };
    })

    return result;
  }

  return null;
}

export default dataService;
