import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome'
import onClickOutside from "react-onclickoutside";
import style from './style.less';

class DropDownBox extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      listOpen: false,
      title: this.props.title
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error != this.state.title) {
      this.setState({ title: nextProps.title });
    }
  }

  handleClickOutside = () => {
    this.setState({
      listOpen: false
    })
  }

  selectItem = (item) => {
    this.setState({
      title: item.title,
      listOpen: false
    }, this.props.onSelect(item))
  }

  toggleList = () => {
    this.setState(prevState => ({
      listOpen: !prevState.listOpen
    }))
  }

  render(){
    const{list} = this.props;
    const{listOpen, title} = this.state;

    return(
      <div className={style.dd_wrapper}>
        <div className={style.drop_down} onClick={this.toggleList}>
          <div className={style.drop_down_title}>
            {title}
          </div>
          <span className={style.fa_search}>
            {listOpen
              ? <FontAwesome name="angle-up" size="2x" className={style.iconSize}/>
              : <FontAwesome name="angle-down" size="2x" className={style.iconSize}/>
            }
          </span>
        </div>
        {listOpen && <ul className={style.dd_list}>
          {list.map((item)=> (
            <li className={style.dd_list_item} key={item.id} onClick={() => this.selectItem(item)}>{item.title} {item.selected && <FontAwesome name="check"/>}</li>
          ))}
        </ul>}
      </div>
    )
  }
}

DropDownBox.propTypes = {
  title: PropTypes.string,
  list: PropTypes.array,
  onSelect: PropTypes.func
};

export default onClickOutside(DropDownBox);
