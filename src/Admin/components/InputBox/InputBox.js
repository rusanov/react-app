import React from 'react';
import PropTypes from 'prop-types';
import style from './style.less';

class InputBox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: props.text,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.text !== nextProps.text) {
      this.setState({ text: nextProps.text });
    }
  }

  onBlur = () => {
    if (this.props.onBlur) {
      this.props.onBlur(this.state.text);
    }
  }

  onChange = (event) => {
    this.setState({text: event.target.value});
  }

  onKeyPress = (event) => {
    if(event.charCode === 13){
        this.onBlur();
    }
  }

  render() {
    return <div className={style.input_box}>
      <div className={style.input_field}>
        <input placeholder={this.props.placeholder} onChange={this.onChange} onKeyPress={this.onKeyPress} value={this.state.text} onBlur={this.onBlur} />
      </div>
    </div>;
  }
}

InputBox.propTypes = {
  placeholder: PropTypes.string,
  text: PropTypes.string,
  onBlur: PropTypes.func
};

export default InputBox;
