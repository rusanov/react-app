import 'whatwg-fetch';
import { stringify as queryString } from 'query-string';

const defaultParams = {
  headers: {
    Accept: 'application/json',
    pragma: 'no-cache',
    'cache-control': 'no-cache'
  },
  credentials: 'same-origin'
};

const commonParams = Object.assign({}, defaultParams, {
  headers: Object.assign({}, defaultParams.headers, {
    'Content-Type': 'application/json'
  })
});

function getConnectingSymbol(url) {
  if (url.includes('?')) {
    return url.indexOf('?') === url.length - 1 ? '' : '&';
  }

  return '?';
}

function getUrlWithConnectionSymbol(url) {
  return `${url}${getConnectingSymbol(url)}`;
}

function getUrlWithParams({ url, data }) {
  return `${getUrlWithConnectionSymbol(url)}${queryString(data)}`;
}

function _handleErrors(response) {
  if (!response.ok) {
    return _getErrorResponse(response);
  }
  return response;
}

function _isJson(response) {
  const contentType = response.headers.get(`Content-Type`) || ``;
  return contentType.includes(`application/json`);
}

function _getErrorResponse(response) {
  const { statusText, status } = response;
  if (_isJson(response)) {
    return response.json()
      .then(data => {
        const { Message: message } = data;
        throw new Error(message || statusText);
      });
  }

  throw new Error(status|| statusText);
}


function _parseJson(response) {
  if (_isJson(response)) {
    return response.json();
  }
  return Promise.resolve();
}


function handleRequest(promise) {
  return promise
    .then(_handleErrors)
    .then(_parseJson);
}

export function get(url, data = {}, options = {}) {
  const urlWithParams = getUrlWithParams({ url, data });
  const requestOptions = Object.assign({}, commonParams, options);

  return handleRequest(fetch(urlWithParams, requestOptions));
}

export default {
  get
};
