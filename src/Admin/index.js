import React from 'react';
import ReactDOM from 'react-dom';
import AddressGetter from './AddressGetter/AddressGetter';

ReactDOM.render(<AddressGetter />, document.getElementById('root'));
